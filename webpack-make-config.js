var webpack = require("webpack");
var path = require("path");
var buildPath = path.resolve(__dirname, "build");
var nodeModulesPath = path.resolve(__dirname, "node_modules");
var TransferWebpackPlugin = require("transfer-webpack-plugin");

module.exports = function(env) {

  var entry = [];
  if (env === "development") {
    entry.push("webpack/hot/dev-server");
    entry.push("webpack/hot/only-dev-server");
  }
  entry.push(path.join(__dirname, "/src/app/index.js"));

  var devServer;
  if (env === "development") {
    devServer = {
      contentBase: "src/public",
      devtool: "eval",
      hot: true,
      inline: true,
      port: 3000
    };
  }

  var plugins = [];
  if (env === "production") {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }));
  }
  if (env === "development") {
    plugins.push(new webpack.HotModuleReplacementPlugin());
  }
  plugins.push(new TransferWebpackPlugin([{ from: "public" }], path.resolve(__dirname, "src")));

  var loaders = [];
  if (env === "development") {
    loaders.push("react-hot");
  }
  loaders.push("babel-loader");

  return {
    entry: entry,
    resolve: {
      extensions: ["", ".js"]
    },
    devServer: devServer,
    devtool: env === "development" ? "eval" : "source-map",
    output: {
      path: buildPath,
      filename: 'index.js'
    },
    plugins: plugins,
    module: {
      loaders: [
        {
          test: /\.(js)$/,
          loaders: loaders,
          exclude: [nodeModulesPath]
        }
      ]
    }
  }
};