# Simple React/Redux example to show how to write tests. #

Uses Mocha, expect and Enzyme.

Configuration for WallabyJS exist but is not necessary.

### Installation ###
```
#!

npm install
```

### To the start the dev server (port 3000) ###
```
#!

npm start
```

### To run the tests ###
```
#!

npm test
```

### To continuously run the tests ###
```
#!

npm run test:watch
```

### To use WallabyJS ###

Download the WallabyJS plugin for your editor from https://wallabyjs.com/ and follow the instructions.
Use the file wallaby.js for the configuration.

### To run eslint ###
```
#!

npm runt lint
```
