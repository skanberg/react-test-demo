var wallabyWebpack = require('wallaby-webpack');
var webpackPostprocessor = wallabyWebpack({
  module: {
    loaders: [
      { test: /\.json$/, loader: 'json' }
    ]
  },
  externals: {
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true
  }
});

module.exports = function (wallaby) {
  return {
    files: [
      { pattern: 'src/app/**/*.js', load: false },
      { pattern: 'src/app/**/__tests__/**/*Spec.js', ignore: true }
    ],
    tests: [
      { pattern: 'src/app/**/__tests__/**/*Spec.js', load: false}
    ],
    compilers: {
      'src/app/**/*.js': wallaby.compilers.babel()
    },
    postprocessor: webpackPostprocessor,
    bootstrap: function () {
      window.__moduleBundler.loadTests();
    }
  };
};