import counter from "../counter";
import { increment, decrement } from "../../actions/actions";
import expect from "expect";

describe("counter", () => {
  it("returns initial state", () => {
    expect(counter()).toBe(0);
  });

  it("increments value", () => {
    expect(counter(1, increment())).toBe(2);
  });

  it("decrements value", () => {
    expect(counter(5, decrement())).toBe(4);
  });
});
