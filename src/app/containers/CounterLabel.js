import { connect } from "react-redux";
import Label from "../components/Label";

function mapStateToProps(state) {
  return {
    text: state
  };
}

export default connect(mapStateToProps)(Label);
