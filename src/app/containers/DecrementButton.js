import { connect } from "react-redux";
import { decrement } from "../actions/actions";
import Button from "../components/Button";

function mapDispatchToProps(dispatch) {
  return {
    onClick() {
      dispatch(decrement());
    }
  };
}

export default connect(null, mapDispatchToProps)(Button);
