import { renderWithStore } from "../../utils/testUtils";
import expect from "expect";
import CounterLabel from "../CounterLabel";

describe("CounterLabel", () => {
  it("maps state to props", () => {
    const state = "count state";
    const component = renderWithStore(CounterLabel, {}, state);

    expect(component.prop("text")).toBe(state);
  });
});
