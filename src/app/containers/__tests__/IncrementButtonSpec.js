import { renderWithStore } from "../../utils/testUtils";
import expect from "expect";
import IncrementButton from "../IncrementButton";
import { increment } from "../../actions/actions";

describe("IncrementButton", () => {
  it("calls increment action", () => {
    const dispatch = expect.createSpy();
    const component = renderWithStore(IncrementButton, {}, {}, dispatch);

    component.prop("onClick")();

    expect(dispatch).toHaveBeenCalledWith(increment());
  });
});
