import expect from "expect";
import { renderWithStore } from "../../utils/testUtils";
import DecrementButton from "../DecrementButton";
import { decrement } from "../../actions/actions";

describe("DecrementButton", () => {
  it("calls decrement action", () => {
    const dispatch = expect.createSpy();
    const component = renderWithStore(DecrementButton, {}, {}, dispatch);

    component.prop("onClick")();

    expect(dispatch).toHaveBeenCalledWith(decrement());
  });
});

