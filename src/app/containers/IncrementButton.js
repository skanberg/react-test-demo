import { connect } from "react-redux";
import { increment } from "../actions/actions";
import Button from "../components/Button";

function mapDispatchToProps(dispatch) {
  return {
    onClick() {
      dispatch(increment());
    }
  };
}

export default connect(null, mapDispatchToProps)(Button);
