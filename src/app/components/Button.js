import React from "react";

export default ({ text, onClick }) => (
  <button className="button" onClick={ onClick }>{ text }</button>
);
