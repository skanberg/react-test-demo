import React from "react";
import IncrementButton from "../containers/IncrementButton";
import DecrementButton from "../containers/DecrementButton";
import CounterLabel from "../containers/CounterLabel";

export default () => (
  <div>
    <CounterLabel />
    <IncrementButton text="Increment" />
    <DecrementButton text="Decrement" />
  </div>
);
