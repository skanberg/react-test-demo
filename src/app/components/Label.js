import React from "react";

export default ({ text }) => (
  <div className="label">{ text }</div>
);
