import React from "react";
import { shallow } from "enzyme";
import expect from "expect";
import Button from "../Button";

describe("Button", () => {
  it("renders component of type button", () => {
    const component = shallow(<Button />);

    expect(component.type()).toBe("button");
  });

  it("displays text", () => {
    const text = "button text";
    const component = shallow(<Button text={text} />);

    expect(component.text()).toBe(text);
  });

  it("it calls onClick function", () => {
    const onClick = expect.createSpy();
    const component = shallow(<Button onClick={onClick} />);

    component.simulate("click");

    expect(onClick).toHaveBeenCalled();
  });

  it("adds correct class", () => {
    const component = shallow(<Button />);

    expect(component.hasClass("button")).toExist();
  });
});
