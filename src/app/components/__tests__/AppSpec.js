import React from "react";
import expect from "expect";
import { shallow } from "enzyme";
import App from "../App";
import IncrementButton from "../../containers/IncrementButton";
import DecrementButton from "../../containers/DecrementButton";
import CounterLabel from "../../containers/CounterLabel";

describe("App", () => {
  it("renders counter label", () => {
    const component = shallow(<App />);

    const counterLabel = component.find(CounterLabel);
    expect(counterLabel.length).toBe(1);
  });

  describe("buttons", () => {
    it("renders increment button", () => {
      const component = shallow(<App />);

      const incrementButton = component.find(IncrementButton);
      expect(incrementButton.length).toBe(1);
      expect(incrementButton.prop("text")).toBe("Increment");
    });

    it("renders decrement component", () => {
      const component = shallow(<App />);

      const decrementButton = component.find(DecrementButton);
      expect(decrementButton.length).toBe(1);
      expect(decrementButton.prop("text")).toBe("Decrement");
    });
  });
});

