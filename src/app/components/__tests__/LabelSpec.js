import React from "react";
import { shallow } from "enzyme";
import expect from "expect";
import Label from "../Label";

describe("Label", () => {
  it("displays text", () => {
    const text = "label text";
    const component = shallow(<Label text={ text } />);

    expect(component.text()).toBe(text);
  });

  it("adds correct class", () => {
    const component = shallow(<Label />);

    expect(component.hasClass("label")).toExist();
  });
});
