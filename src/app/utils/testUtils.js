import React from "react";
import { shallow } from "enzyme";

export function renderWithStore(component, props = {}, state = {}, dispatch = () => ({})) {
  const context = {
    store: {
      getState: () => state,
      subscribe: () => ({}),
      dispatch
    }
  };
  return shallow(React.createElement(component, props), { context });
}
