import React from "react";
import { render } from "react-dom";
import { createStore, compose } from "redux";
import { Provider } from "react-redux";
import App from "./components/App";
import counter from "./reducers/counter";

function getDevToolsExtension() {
  return window.devToolsExtension ? window.devToolsExtension() : f => f;
}

const store = createStore(counter, compose(getDevToolsExtension()));

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
