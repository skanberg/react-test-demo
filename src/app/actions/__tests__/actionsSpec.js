import expect from "expect";
import { increment, decrement, INCREMENT, DECREMENT } from "../actions";

describe("actions", () => {
  describe("increment", () => {
    it("returns correct type", () => {
      expect(increment()).toEqual({
        type: INCREMENT
      });
    });
  });

  describe("decrement", () => {
    it("returns correct type", () => {
      expect(decrement()).toEqual({
        type: DECREMENT
      });
    });
  });
});
